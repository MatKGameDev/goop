#include "AudioLayer.h"
#include "AudioEngine.h"

void AudioLayer::Initialize()
{
	AudioEngine& audioEngine = AudioEngine::GetInstance();

	audioEngine.Init();
	audioEngine.LoadBank("Master");

	//audioEngine.LoadEvent("Music");
	//audioEngine.PlayEvent("Music");
}

void AudioLayer::Shutdown()
{
	AudioEngine::GetInstance().Shutdown();
}

void AudioLayer::Update()
{
	AudioEngine::GetInstance().Update();
}

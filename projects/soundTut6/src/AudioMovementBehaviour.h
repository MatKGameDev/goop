#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "florp/app/Window.h"
#include "florp/app/Application.h"
#include "AudioEngine.h"


class AudioMovementBehaviour : public florp::game::IBehaviour {
public:
	AudioMovementBehaviour() : IBehaviour(), 
		newPosition(glm::vec3(0.0f)), 
		angle(4.7f), 
		radius(10.0f), 
		angleIncrement(1.0f),
		radiusIncrement(1.0f),
		isAngleMoving(false), 
		isRadiusMoving(false) {};

	virtual ~AudioMovementBehaviour() = default;

	virtual void OnLoad(entt::entity entity) override {
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);

		// ooo ooo ahh ahh
		AudioEngine& audioEngine = AudioEngine::GetInstance();
		audioEngine.LoadEvent("Monkey");
		audioEngine.PlayEvent("Monkey");

		audioEngine.SetListenerPosition(transform.GetLocalPosition());

		audioEngine.SetListenerOrientation(transform.GetUp(), transform.GetForward());
	}

	virtual void Update(entt::entity entity) override {
		using namespace florp::app;
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		Window::Sptr window = Application::Get()->GetWindow();

		
		// Input 
		if (window->IsKeyDown(Key::P)) {
			isAngleMoving = !isAngleMoving;
		}

		if (window->IsKeyDown(Key::O)) {
			isRadiusMoving = !isRadiusMoving;
		}
		
		if (window->IsKeyDown(Key::R)) {
			angle = 4.7f;
			radius = 10.0f;
		}

		// TODO: Move the monkey head in a circle
		if (isAngleMoving)
		{
			angle += angleIncrement * florp::app::Timing::DeltaTime;
		}

		// TODO: Move the monkey head forward and back
		if (isRadiusMoving)
		{
			radius += radiusIncrement * florp::app::Timing::DeltaTime;

			if (radius < 10.0f || radius > 30.0f)
				radiusIncrement = -radiusIncrement;
		}

		newPosition.x = cos(angle) * radius;
		newPosition.z = sin(angle) * radius;

		transform.SetPosition(newPosition);
		AudioEngine::GetInstance().SetEventPosition("Monkey", newPosition);

	}

private:
	glm::vec3 newPosition;
	
	float angle;
	float radius;

	float angleIncrement;
	float radiusIncrement;

	bool isAngleMoving;
	bool isRadiusMoving;
};
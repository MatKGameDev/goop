#pragma once
#define GLM_ENABLE_EXPERIMENTAL

#include "florp/game/IBehaviour.h"
#include "Logging.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "imgui.h"
#include "GLM/gtx/rotate_vector.hpp"
#include "florp/game/Material.h"

struct LightComponent
{
	int lightNum = -1;
	florp::game::Material::Sptr linkedMat;
};

class LightMoveBehaviour : public florp::game::IBehaviour {
public:
	LightMoveBehaviour(const float speed) : IBehaviour(), mySpeed(speed) {};
	virtual ~LightMoveBehaviour() = default;

	virtual void Update(entt::entity entity) override {
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		glm::vec3 currentPosition = transform.GetLocalPosition();
		transform.SetPosition(glm::rotate(currentPosition, mySpeed * florp::app::Timing::DeltaTime, glm::vec3(0.f, 1.f, 0.f)));

		auto& light = CurrentRegistry().get<LightComponent>(entity);

		if (light.lightNum == 0)
			light.linkedMat->Set("a_Lights[0].Pos", transform.GetLocalPosition());
		else if (light.lightNum == 1)
			light.linkedMat->Set("a_Lights[1].Pos", transform.GetLocalPosition());
	}

private:
	float mySpeed;
};
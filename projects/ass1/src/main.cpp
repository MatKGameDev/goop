#include "florp/app/Application.h"
#include "florp/game/BehaviourLayer.h"
#include "SceneBuildLayer.h"
#include "florp/game/ImGuiLayer.h"
#include <RenderLayer.h>
#include <PostLayer.h>
#include <cameraControlBehaviour.h>

int main()
{
	// Create our application
	florp::app::Application* app = new florp::app::Application();

	// Set up our layers
	app->AddLayer<florp::game::BehaviourLayer>();
	app->AddLayer<florp::game::ImGuiLayer>();
	app->AddLayer<SceneBuilder>();
	app->AddLayer<RenderLayer>();
	app->AddLayer<PostLayer>();
	
	app->Run();
	
	return 0;
} 

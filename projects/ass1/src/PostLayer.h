#pragma once
#include "florp/app/ApplicationLayer.h"
#include "FrameBuffer.h"
#include "florp/graphics/Mesh.h"
#include "florp/graphics/Shader.h"
#include "florp/app/Application.h"
#include "florp/game/SceneManager.h"

class PostLayer : public florp::app::ApplicationLayer
{
public:
	PostLayer();
	virtual void OnWindowResize(uint32_t width, uint32_t height) override;
	virtual void PostRender() override;
	
	void addLayer(const char* vertexShaderPath, const char*fragmentShaderPath, RenderBufferDesc renderBuffer);

protected:
	florp::graphics::Mesh::Sptr myFullscreenQuad;
	struct PostPass {
		florp::graphics::Shader::Sptr Shader;
		FrameBuffer::Sptr Output;
	};
	std::vector<PostPass> myPasses;
};
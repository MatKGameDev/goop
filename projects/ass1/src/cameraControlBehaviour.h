#pragma once
#include "florp/game/IBehaviour.h"
#include "Logging.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "imgui.h"
#include "florp/app/Window.h"
#include "GLFW/glfw3.h"
#include "florp/app/Application.h"
#include "CameraComponent.h"

class cameraControlBehaviour : public florp::game::IBehaviour {

public:
	cameraControlBehaviour(const float& speed) :
		IBehaviour(), camSpeed(speed)
	{};


	virtual ~cameraControlBehaviour() = default;

	virtual void Update(entt::entity entity) override {
		florp::app::Application* app = florp::app::Application::Get();

		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		florp::app::WindowKeyEventCallback;

		glm::mat4 inverted = glm::inverse(transform.GetWorldTransform());

		glm::vec3 up      = normalize(glm::vec3(inverted[1]) * glm::vec3(1.f, 1.f, -1.f));
		glm::vec3 forward = normalize(glm::vec3(inverted[2]) * glm::vec3(1.f, 1.f, -1.f));
		glm::vec3 right   = glm::cross(up, forward) * glm::vec3(-1.f, -1.f, -1.f);

		if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_W) == GLFW_PRESS)
		{
			auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
			transform.SetPosition(transform.GetLocalPosition() + forward * camSpeed * florp::app::Timing::DeltaTime);
		}

		if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_S) == GLFW_PRESS)
		{
			auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
			transform.SetPosition(transform.GetLocalPosition() - forward * camSpeed * florp::app::Timing::DeltaTime);
		}

		if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_A) == GLFW_PRESS)
		{
			auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
			transform.SetPosition(transform.GetLocalPosition() - right * camSpeed * florp::app::Timing::DeltaTime);
		}

		if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_D) == GLFW_PRESS)
		{
			auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
			transform.SetPosition(transform.GetLocalPosition() + right * camSpeed * florp::app::Timing::DeltaTime);
		}

		if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
		{
			auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
			transform.SetPosition(transform.GetLocalPosition() - up * camSpeed * florp::app::Timing::DeltaTime);
		}

		if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_SPACE) == GLFW_PRESS)
		{
			auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
			transform.SetPosition(transform.GetLocalPosition() + up * camSpeed * florp::app::Timing::DeltaTime);
		}

		if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_LEFT) == GLFW_PRESS)
		{
			auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
			transform.Rotate(glm::vec3(0.0f, camSpeed * florp::app::Timing::DeltaTime * 5.0f, 0.0f));
		}
		if (glfwGetKey(app->GetWindow()->GetHandle(), GLFW_KEY_RIGHT) == GLFW_PRESS)
		{
			auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
			transform.Rotate(glm::vec3(0.0f, -camSpeed * florp::app::Timing::DeltaTime * 5.0f, 0.0f));
		}

	}
private:
	float camSpeed;
};
#include "SceneBuildLayer.h"
#include "florp/game/SceneManager.h"
#include "florp/game/RenderableComponent.h"
#include <florp\graphics\MeshData.h>
#include <florp\graphics\MeshBuilder.h>
#include <florp\graphics\ObjLoader.h>
#include <glm/gtc/matrix_transform.hpp>
#include <florp\game\Transform.h>
#include <florp\app\Application.h>
#include <SampleBehaviour.h>
#include <LightMoveBehaviour.h>
#include <cameraControlBehaviour.h>
#include "ObjectMoveBehaviour.h"

#include "CameraComponent.h"

void SceneBuilder::Initialize()
{
	using namespace florp::game;
	using namespace florp::graphics;
	
	auto* scene = SceneManager::RegisterScene("main");
	SceneManager::SetCurrentScene("main");

	Shader::Sptr shader = std::make_shared<Shader>();
	shader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	shader->LoadPart(ShaderStageType::FragmentShader, "shaders/blinn-phong-multi.fs.glsl");
	shader->Link();
	
	Material::Sptr mat = std::make_shared<Material>(shader);
	mat->Set("a_AmbientColor", { 1.0f, 1.0f, 1.0f });
	mat->Set("a_AmbientPower", 0.001f);

	entt::entity light0Entity = scene->CreateEntity();
	auto& light0 = scene->Registry().assign<LightComponent>(light0Entity);
	light0.lightNum = 0;
	light0.linkedMat = mat;
	scene->AddBehaviour<LightMoveBehaviour>(light0Entity, 2.f);
	Transform& t0 = scene->Registry().get<Transform>(light0Entity);
	t0.SetPosition(glm::vec3(-5, 0, 0));

	mat->Set("a_Lights[0].Pos", t0.GetLocalPosition());
	mat->Set("a_Lights[0].Color", { 0.6f, 0.0f, 0.2f });
	mat->Set("a_Lights[0].Attenuation", 1.0f / 100.0f);

	entt::entity light1Entity = scene->CreateEntity();
	auto& light1 = scene->Registry().assign<LightComponent>(light1Entity);
	light1.lightNum = 1;
	light1.linkedMat = mat;
	scene->AddBehaviour<LightMoveBehaviour>(light1Entity, 1.f);
	Transform& t1 = scene->Registry().get<Transform>(light1Entity);
	t1.SetPosition(glm::vec3(-5, 0, 0));

	mat->Set("a_Lights[1].Pos", t1.GetLocalPosition());
	mat->Set("a_Lights[1].Color", { 0.0f, 0.4f, 0.4f });
	mat->Set("a_Lights[1].Attenuation", 1.0f / 100.0f);

	mat->Set("a_EnabledLights", 2);

	//Plane
	MeshData planeMesh = ObjLoader::LoadObj("plane.obj", glm::vec4(1.0f));
	entt::entity planeEntt = scene->CreateEntity();
	RenderableComponent& renderablePlane = scene->Registry().assign<RenderableComponent>(planeEntt);
	renderablePlane.Mesh = MeshBuilder::Bake(planeMesh);
	renderablePlane.Material = mat;
	Transform& tPlane = scene->Registry().get<Transform>(planeEntt);
	tPlane.SetPosition(glm::vec3(0, 0, -9));
	tPlane.SetScale(3.0f);

	//Cloth
	MeshData clothMesh = ObjLoader::LoadObj("cloth.obj", glm::vec4(1.0f));
	entt::entity clothEntt = scene->CreateEntity();
	RenderableComponent& renderableCloth = scene->Registry().assign<RenderableComponent>(clothEntt);
	renderableCloth.Mesh = MeshBuilder::Bake(clothMesh);
	renderableCloth.Material = mat;
	Transform& tCloth = scene->Registry().get<Transform>(clothEntt);
	tCloth.SetPosition(glm::vec3(0, 3, -8));

	scene->AddBehaviour<SampleBehaviour>(clothEntt, glm::vec3(70.0f, 40.0f, 70.0f));
	scene->AddBehaviour<ObjectMoveBehaviour>(clothEntt, 6.f, glm::vec3(2.f, 4.f, -10.f));

	//Cone
	MeshData coneMesh = ObjLoader::LoadObj("cone.obj", glm::vec4(1.0f));
	entt::entity coneEntt = scene->CreateEntity();
	RenderableComponent& renderableCone = scene->Registry().assign<RenderableComponent>(coneEntt);
	renderableCone.Mesh = MeshBuilder::Bake(coneMesh);
	renderableCone.Material = mat;
	Transform& tCone = scene->Registry().get<Transform>(coneEntt);
	tCone.SetPosition(glm::vec3(2, 2, -3));
	tCone.SetScale(glm::vec3(2.0f, 2.0f, 2.0f));

	scene->AddBehaviour<ObjectMoveBehaviour>(coneEntt, 4.f, glm::vec3(0.f, 3.f, 0.f));

	//Cube
	MeshData cubeMesh = ObjLoader::LoadObj("cube.obj", glm::vec4(1.0f));
	entt::entity cubeEntt = scene->CreateEntity();
	RenderableComponent& renderableCube = scene->Registry().assign<RenderableComponent>(cubeEntt);
	renderableCube.Mesh = MeshBuilder::Bake(cubeMesh);
	renderableCube.Material = mat;
	//scene->AddBehaviour<SampleBehaviour>(test, glm::vec3(0.0f, 7.0f, 0.0f));
	Transform& tCube = scene->Registry().get<Transform>(cubeEntt);
	tCube.SetPosition(glm::vec3(-3, 3, -4));
	tCube.SetEulerAngles(glm::vec3(0.0f, 30.0f, 0.0f));

	scene->AddBehaviour<SampleBehaviour>(cubeEntt, glm::vec3(80.0f, 50.0f, 80.0f));
	scene->AddBehaviour<ObjectMoveBehaviour>(cubeEntt, 7.f, glm::vec3(-1.f, 4.f, -2.f));
	
	{
		florp::app::Application* app = florp::app::Application::Get();
		// The color buffer should be marked as shader readable, so that we generate a texture for it
		RenderBufferDesc mainColor = RenderBufferDesc();
		mainColor.ShaderReadable = true;
		mainColor.Attachment = RenderTargetAttachment::Color0;
		mainColor.Format = RenderTargetType::Color24;

		// The depth attachment does not need to be a texture (and would cause issues since the format is DepthStencil)
		RenderBufferDesc depth = RenderBufferDesc();
		depth.Attachment = RenderTargetAttachment::DepthStencil;
		depth.Format = RenderTargetType::DepthStencil;

		// Our main frame buffer needs a color output, and a depth output
		FrameBuffer::Sptr buffer = std::make_shared<FrameBuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		buffer->AddAttachment(mainColor);
		buffer->AddAttachment(depth);
		buffer->Validate();

		entt::entity camera = scene->CreateEntity();
		CameraComponent& cam = scene->Registry().assign<CameraComponent>(camera);
		cam.Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);
		cam.Buffer = buffer;
		cam.IsMainCamera = true;
		Transform& t = scene->Registry().get<Transform>(camera);
		t.SetPosition(glm::vec3(0.0f, 4.0f, 12.0f));
		scene->AddBehaviour<cameraControlBehaviour>(camera, 5.0f);
	}
}

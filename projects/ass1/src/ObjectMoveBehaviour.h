#pragma once
#define GLM_ENABLE_EXPERIMENTAL

#include "florp/game/IBehaviour.h"
#include "Logging.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"
#include "imgui.h"
#include "GLM/gtx/rotate_vector.hpp"
#include "florp/game/Material.h"

class ObjectMoveBehaviour : public florp::game::IBehaviour {
public:
	ObjectMoveBehaviour(const float speed, glm::vec3 rotationPoint) : IBehaviour(), mySpeed(speed), myRotationPoint(rotationPoint) {};
	virtual ~ObjectMoveBehaviour() = default;

	virtual void Update(entt::entity entity) override {
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		
		glm::vec3 currentPosition = transform.GetLocalPosition();
		transform.SetPosition(glm::rotate(currentPosition, mySpeed * florp::app::Timing::DeltaTime, myRotationPoint));
	}

private:
	float mySpeed;
	glm::vec3 myRotationPoint;
};
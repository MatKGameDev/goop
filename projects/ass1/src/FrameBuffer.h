#pragma once
#include "EnumToString.h"
#include "florp/graphics/BufferLayout.h"
#include "florp/graphics/ITexture.h"
#include "florp/graphics/Texture2D.h"
#include "glad/glad.h"
#include "GLM/glm.hpp"

ENUM(RenderTargetAttachment, uint32_t,
	Color0 = GL_COLOR_ATTACHMENT0,
	Color1 = GL_COLOR_ATTACHMENT1,
	Color2 = GL_COLOR_ATTACHMENT2,
	Color3 = GL_COLOR_ATTACHMENT3,
	Color4 = GL_COLOR_ATTACHMENT4,
	Color5 = GL_COLOR_ATTACHMENT5,
	Color6 = GL_COLOR_ATTACHMENT6,
	Color7 = GL_COLOR_ATTACHMENT7,
	Depth = GL_DEPTH_ATTACHMENT,
	DepthStencil = GL_DEPTH_STENCIL_ATTACHMENT,
	Stencil = GL_STENCIL_ATTACHMENT
);

ENUM(RenderTargetType, uint32_t,

	Color256F = GL_RGBA16F,
	Color192F = GL_RGB16F,
	Color128F = GL_RG16F,
	Color64F = GL_R16F,

	Color32 = GL_RGBA8,
	Color24 = GL_RGB8,
	Color16 = GL_RG8,
	Color8 = GL_R8,
	DepthStencil = GL_DEPTH24_STENCIL8,
	Depth16 = GL_DEPTH_COMPONENT16,
	Depth24 = GL_DEPTH_COMPONENT24, 
	Depth32 = GL_DEPTH_COMPONENT32,
	Stencil4 = GL_STENCIL_INDEX4,
	Stencil8 = GL_STENCIL_INDEX8,
	Stencil16 = GL_STENCIL_INDEX16,
	Default = 0
);

ENUM_FLAGS(RenderTargetBinding, GLenum,
	None = 0,
	Draw = GL_DRAW_FRAMEBUFFER,
	Write = GL_DRAW_FRAMEBUFFER,
	Read = GL_READ_FRAMEBUFFER,
	Both = GL_FRAMEBUFFER
);
ENUM_FLAGS(BufferFlags, GLenum,
	None = 0,
	Color = GL_COLOR_BUFFER_BIT,
	Depth = GL_DEPTH_BUFFER_BIT,
	Stencil = GL_STENCIL_BUFFER_BIT,
	All = Color | Depth | Stencil
);

struct RenderBufferDesc {
	/*
	* If this is set to true, we will generate an OpenGL texture instead of a render buffer
	*/
	bool ShaderReadable;
	/*
	* The format internal format of the render buffer or texture
	*/
	RenderTargetType Format;
	/*
	* Where the buffer will be attached to
	*/
	RenderTargetAttachment Attachment;
};

class FrameBuffer : public florp::graphics::ITexture {
public:
	typedef std::shared_ptr<FrameBuffer> Sptr;
	FrameBuffer(uint32_t width, uint32_t height, uint8_t numSamples = 1);
	virtual ~FrameBuffer();

	uint32_t GetWidth() const { return myWidth; }
	uint32_t GetHeight() const { return myHeight; }
	glm::ivec2 GetSize() const { return { myWidth, myHeight }; }

	void Resize(uint32_t newWidth, uint32_t newHeight);

	void AddAttachment(const RenderBufferDesc& desc); //Adds a new attachment to this FrameBuffer
	florp::graphics::Texture2D::Sptr GetAttachment(RenderTargetAttachment attachment); //Gets the attachment if it's a texture
	
	bool Validate(); //Check if this Framebuffer is ready to go

	virtual void Bind(uint32_t slot) override; //ITexture override
	void Bind(RenderTargetBinding bindMode = RenderTargetBinding::Draw) const; //Binds this FrameBuffer for reading or writing
	void UnBind() const;

	static void Blit(
		const glm::ivec4& srcBounds, const glm::ivec4& dstBounds,
		BufferFlags flags = BufferFlags::All, florp::graphics::MagFilter filterMode = florp::graphics::MagFilter::Linear);

protected:
	//The dimensions of this frame buffer
	uint32_t myWidth, myHeight;
	//The number of samples to use if we have multisampling enabled
	uint8_t myNumSamples;
	//Whether or not this frame buffer is in a valid state
	bool isValid;
	//The current attachment points that this FrameBuffer is bound to
	mutable RenderTargetBinding myBinding;
	//Stores our attachment information for a given render buffer attachment point
	struct RenderBuffer {
		GLuint RendererID;
		florp::graphics::IGraphicsResource::Sptr Resource;
		bool IsRenderBuffer;
		RenderBufferDesc Description;
		RenderBuffer();
	};
	//Stores our buffers per attachment point
	std::unordered_map<RenderTargetAttachment, RenderBuffer> myLayers;
};
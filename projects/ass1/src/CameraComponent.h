#pragma once
#include <optional>
#include <memory>
#include <GLM/glm.hpp>
#include <FrameBuffer.h>

/*
 * Stores the information required to render with a camera. Since this is a component of a gameobject,
 * we do not need a view matrix (we can take the inverse of the matrix that it is attached to).
 *
 * Later on, we can add things like render targets for the camera to render to
 */
struct CameraComponent {
	// True if this camera is the main camera for the scene (ie, what gets rendered fullscreen)
	bool              IsMainCamera;
	// The Color to clear to when using this camera
	glm::vec4         ClearCol = glm::vec4(0.5f, 0.5f, 0.5f, 1);

	//Render target for the camera
	FrameBuffer::Sptr Buffer;
	
	// The projection matrix for this camera
	glm::mat4         Projection;
};
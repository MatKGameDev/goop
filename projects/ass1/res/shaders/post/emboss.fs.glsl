#version 440
layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;
layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;

void main()
{
    const float offset = 1.0/300.0;

    vec2 offsets[9] = vec2[](
        vec2(-offset,  offset), // top-left
        vec2( 0.0f,    offset), // top-center
        vec2( offset,  offset), // top-right
        vec2(-offset,  0.0f),   // center-left
        vec2( 0.0f,    0.0f),   // center-center
        vec2( offset,  0.0f),   // center-right
        vec2(-offset, -offset), // bottom-left
        vec2( 0.0f,   -offset), // bottom-center
        vec2( offset, -offset)  // bottom-right    
    );

    float embossKernel[9] = float[](
            4,    0,   0,
            1,    0,   0,
            1,    0,  -4
    );
    
    vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
        sampleTex[i] = vec3(texture(xImage, inUV.st + offsets[i]));
    }

    vec3 col = vec3(0.0);

    for(int i = 0; i < 9; i++)
        col += sampleTex[i] * embossKernel[i];
    
    outColor = vec4(col, 1.0);
}